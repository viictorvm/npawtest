package entity;

import java.util.List;

public class ServiceDataObject {
    private String accountCode;
    private String targetDevice;
    private String pluginVersion;
    private Integer pingTime;
    private Integer serverA;
    private Integer serverB;

    public ServiceDataObject(String accountCode, String targetDevice, String pluginVersion, Integer pingTime, Integer serverA, Integer serverB) {
        this.accountCode = accountCode;
        this.targetDevice = targetDevice;
        this.pluginVersion = pluginVersion;
        this.pingTime = pingTime;
        this.serverA = serverA;
        this.serverB = serverB;
    }

    public ServiceDataObject(List<String> configuration) {
        this(configuration.get(0), configuration.get(1), configuration.get(2), Integer.parseInt(configuration.get(3)), Integer.parseInt(configuration.get(4)), Integer.parseInt(configuration.get(5)));
    }

    public String accountCode() {
        return accountCode;
    }

    public String targetDevice() {
        return targetDevice;
    }

    public int pingTime() {
        return pingTime;
    }

    public String randomServer() {
        double randomPercentage = Math.random() * 100;
        if(serverB > randomPercentage) {
            return "clusterB";
        }
        return "clusterA";
    }
}

