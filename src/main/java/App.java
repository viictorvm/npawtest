import services.CacheService;
import handlers.MainHandler;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.RoutingHandler;
import io.undertow.util.Methods;

import java.util.Timer;
import java.util.TimerTask;

public class App {
    private static final HttpHandler ROUTES = new RoutingHandler().add(Methods.GET, "/getData*", new MainHandler());
    private static final Integer MILISECONDS_LOAD_CONFIG = 5000;
    private static final Integer TIME_DELAY_EXECUTION = 0;
    private static final Integer APP_PORT = 80;
    private static final String APP_HOST = "localhost";

    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                CacheService.getInstance().loadConfigFromFile();
            }
        }, TIME_DELAY_EXECUTION, MILISECONDS_LOAD_CONFIG);

        Undertow server = Undertow.builder()
                .addHttpListener(APP_PORT, APP_HOST)
                .setHandler(ROUTES)
                .build();
        server.start();
    }
}
