package handlers;

import services.DataService;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

import java.util.Deque;
import java.util.Map;


public class MainHandler implements HttpHandler {
    private DataService dataService = new DataService();
    private String result = "";

    @Override
    public void handleRequest(final HttpServerExchange exchange) {
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/xml");
        Map<String, Deque<String>> parameters = exchange.getQueryParameters();
        boolean allParams = parameters.containsKey("accountCode") &&
                parameters.containsKey("targetDevice") &&
                parameters.containsKey("pluginVersion");

        if (allParams) {
            String accountCode = parameters.get("accountCode").getFirst();
            String targetDevice = parameters.get("targetDevice").getFirst();
            String pluginVersion = parameters.get("pluginVersion").getFirst();

            result = this.dataService.execute(accountCode, targetDevice, pluginVersion);
        }

        exchange.getResponseSender().send(result);
    }
}
