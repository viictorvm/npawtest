package services;

import entity.ServiceDataObject;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class CacheService {

    private Properties prop;
    private FileInputStream fileInputStream;
    private HashMap<String, ArrayList<ServiceDataObject>> fileConfiguration = new HashMap<String, ArrayList<ServiceDataObject>>();

    private final String FILE_CONFIG_PATH = "./src/main/resources/config.properties";
    private static CacheService CACHE_SERVICE_INSTANCE = new CacheService();

    private CacheService() {
        this.loadConfigFromFile();
    }

    public synchronized void loadConfigFromFile() {
        try {
            this.fileConfiguration.clear();
            this.fileInputStream = new FileInputStream(FILE_CONFIG_PATH);
            this.prop = new Properties();
            this.prop.load(fileInputStream);

            this.readVariableNameFromFileToCache(new String[]{"firstConfiguration", "secondConfiguration"});
            this.readVariableNameFromFileToCache(new String[]{"thirdConfiguration"});

            this.prop.clear();
            this.fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readVariableNameFromFileToCache(String[] fileVariableNameArray) {
        ArrayList<ServiceDataObject> serviceDataPerAccountCode = new ArrayList<ServiceDataObject>();
        String keyName = null;

        for (String variableName : fileVariableNameArray) {
            String clientProperties = this.prop.getProperty(variableName);
            List<String> myList = new ArrayList<String>(Arrays.asList(clientProperties.split(",")));
            ServiceDataObject currentConfiguration = new ServiceDataObject(myList);
            serviceDataPerAccountCode.add(currentConfiguration);
            keyName = currentConfiguration.accountCode();
        }

        if (!serviceDataPerAccountCode.isEmpty()) {
            this.fileConfiguration.put(keyName, serviceDataPerAccountCode);
        }
    }

    public ArrayList<ServiceDataObject> getCacheByAccountCode(String accountCode) {
        return this.fileConfiguration.get(accountCode);
    }

    public static CacheService getInstance() {
        return CACHE_SERVICE_INSTANCE;
    }
}
