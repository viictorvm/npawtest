package services;

import dataTransformer.ConvertToXML;
import entity.ServiceDataObject;

import java.util.ArrayList;
import java.util.UUID;


public class DataService {
    private CacheService cacheService = CacheService.getInstance();
    private ServiceDataObject serviceFoundCached;
    private String output;

    public String execute(String accountCode, String targetDevice, String pluginVersion) {
        this.serviceFoundCached = null;
        this.output = "";
        ArrayList<ServiceDataObject> serviceDataCached = this.cacheService.getCacheByAccountCode(accountCode);

        if (null != serviceDataCached && !serviceDataCached.isEmpty()) {
            for (ServiceDataObject currentServiceDataCached : serviceDataCached) {
                if (targetDevice.equals(currentServiceDataCached.targetDevice())) {
                    serviceFoundCached = currentServiceDataCached;
                }
            }
        }

        if (null != serviceFoundCached) {
            String viewCode = UUID.randomUUID().toString().replace("-", "");
            output = ConvertToXML.parse(serviceFoundCached.randomServer(), serviceFoundCached.pingTime(), viewCode);
        }

        return output;
    }
}
