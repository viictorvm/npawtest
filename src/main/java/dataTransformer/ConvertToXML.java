package dataTransformer;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;

public class ConvertToXML {

    public static String parse(String cluster, Integer pingTime, String viewCode) {
        String result = "";

        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element root = document.createElement("q");
            document.appendChild(root);

            Element h = document.createElement("h");
            h.appendChild(document.createTextNode(cluster));
            root.appendChild(h);

            Element pt = document.createElement("pt");
            pt.appendChild(document.createTextNode(String.valueOf(pingTime)));
            root.appendChild(pt);

            Element c = document.createElement("c");
            c.appendChild(document.createTextNode(viewCode));
            root.appendChild(c);

            document.setXmlStandalone(true);

            StringWriter writer = new StringWriter();
            StreamResult streamResult = new StreamResult(writer);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            transformer.transform(domSource, streamResult);
            result = writer.toString();

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }

        return result;
    }
}
