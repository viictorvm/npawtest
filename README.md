# DataService kata
Service that listens HTTP requests on port 80 of the different plugins 
that are distributed in different devices of our clients, 
and responds mainly a unique code that will identify the view, 
the ping time that the device will use, and the host to which you must send all the information the plugin. 
The main feature of the service is to respond very fast, and accept as many concurrent calls as possible.

The first thing to do is to identify if the Account belongs to the platform, if it is positive, the execution will continue, otherwise it will return an empty response.

In the second case, it will check if the sent device for the defined Account is accepted, as in the previous step, in the positive case the execution will continue, otherwise it will return an empty response.

The version of the plugin will also be used to define the value of the pingTime that will be sent to the plugin.

![DataService Rules](rules.png)

*Tips*: Avoid to hardcode the current configuration for each service and will be a bonus for hot reloading.

***Example*** -> http://dataservice.com/getData?accountCode=clienteA&targetDevice=XBox&pluginVersion=3.3.1

## Output

The value of the tag h defines the host (or cluster) to which all the information will be sent by the plugin, 
the pt tag defines the pingTime (which by default will be 5, but will depend on the pluginVersion), 
and finally the tag c defines the code of the view (unique code).


```xml
<?xml version='1.0'encoding='UTF-8'?>
<q>
  <h>clusterA.com</h>
  <pt>5</pt>
  <c>7xnj85f06yqswc5x</c>
</q>
```

## Tools
1. [Maven](https://maven.apache.org/) for depencies.
2. [Undertow](http://undertow.io/) as server.
